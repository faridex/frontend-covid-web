import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private SERVER_URL = "https://covid-3scale-apicast-staging.apps.fort-980c.open.redhat.com:443/?user_key=a4cd763313a6a1beedb014dfe1c41210";
  //"http://backend-map-covid.apps.fort-980c.open.redhat.com";//"http://localhost:8080";
  
  constructor(private httpClient: HttpClient) { }

  public fetchData(){  
    return this.httpClient.get(`${this.SERVER_URL}`);  
  } 

}