import { Component, OnInit } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { ApiService } from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  localizacoes = [];

  title = 'CONVID LOCAIS';
  lat: number = 0;
  lng: number = 0;
  

  updateMap(){
    this.apiService.fetchData().subscribe((data: any[])=>{  
			this.localizacoes = data;  
    })  
}

  constructor(private apiService: ApiService) { }
	ngOnInit() {
		this.apiService.fetchData().subscribe((data: any[])=>{  
			  
      this.localizacoes = data;  
      /*this.localizacoes.forEach(function(value){
      
      })*/
      
      
		})  
	} 

  
}