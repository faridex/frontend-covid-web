# Estagio 1 - Será responsavel em construir nossa aplicação
FROM node as build-step
WORKDIR /opt
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build

# Estagio 2 - Será responsavel por expor a aplicação
FROM nginx as prod-stage
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
COPY --from=build-step /opt/dist/front-map /usr/share/nginx/html
EXPOSE 80
#CMD ["nginx", "-g", "daemon off;"]
